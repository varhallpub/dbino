<?php

namespace Varhall\Dbino;

use Varhall\Utilino\Collections\ICollection;

/**
 * Nette Database extended class for collection representation
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class Collection extends \Nette\Database\Table\Selection implements ICollection
{    
    use CollectionTrait;
    
    /// MAGIC METHODS
    
    public function __construct(\Nette\Database\Table\Selection $selection, $model, array $plugins = NULL)
    {
        parent::__construct($selection->context, $selection->conventions, $selection->name, NULL);

        $this->cache = $selection->cache;
        $this->model = $model;
        
        if ($plugins)
            $this->plugins = $plugins;
    }
}
