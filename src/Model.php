<?php

namespace Varhall\Dbino;

use Nette\Database\Table\Selection;
use Varhall\Dbino\Events\DeleteArgs;
use Varhall\Dbino\Events\InsertArgs;
use Varhall\Dbino\Events\RestoreArgs;
use Varhall\Dbino\Events\UpdateArgs;
use Varhall\Utilino\ISerializable;

/**
 * Base database model class
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
abstract class Model extends \Nette\Database\Table\ActiveRow implements ISerializable
{
    /**
     * @var \Nette\Database\Context
     */
    protected static $context = NULL;

    /**
     * @var \Nette\DI\Container
     */
    protected static $container = NULL;

    protected $attributes   = [];

    protected $events       = [];


    ////////////////////////////////////////////////////////////////////////////
    /// MAGIC METHODS                                                        ///
    ////////////////////////////////////////////////////////////////////////////

    public function __construct()
    {
        $this->on('creating', function($args) { $this->raise('saving', $args); });
        $this->on('updating', function($args) { $this->raise('saving', $args); });

        $this->on('created', function($args) { $this->raise('saved', $args); });
        $this->on('updated', function($args) { $this->raise('saved', $args); });

        $this->setup();
    }

    public function __isset($name)
    {
        if (isset($this->attributes[$name]))
            return TRUE;

        if (!$this->isSaved())
            return FALSE;

        return parent::__isset($name);
    }

    public function &__get($key)
    {
        // call method with attribute name if exists
        if (method_exists($this, $key)) {
            $value = call_user_func([$this, $key]);
            return $value;
        }

        $camelKey = $this->toCamelCase($key);

        // call camel case method with attribute name if exists
        if (method_exists($this, $camelKey)) {
            $value = call_user_func([$this, $camelKey]);
            return $value;
        }

        $value = $this->getAttribute($key);

        // call mutator getter if exists
        $mutator = 'get' . ucfirst($key) . 'Attribute';
        if (method_exists($this, $mutator)) {
            $value = call_user_func([$this, $mutator], $value);
            return $value;
        }

        return $value;
    }

    public function __set($name, $value)
    {
        $camelName = $this->toCamelCase($name);

        // call mutator getter if exists
        $mutator = 'set' . ucfirst($camelName) . 'Attribute';
        if (method_exists($this, $mutator)) {
            call_user_func([$this, $mutator], $value);

            // set value
        } else {
            $this->setAttribute($name, $value);
        }
    }




    ////////////////////////////////////////////////////////////////////////////
    /// STATIC METHODS                                                       ///
    ////////////////////////////////////////////////////////////////////////////

    public static function setContext(\Nette\Database\Context $context)
    {
        self::$context = $context;
    }

    public static function setContainer(\Nette\DI\Container $container)
    {
        self::$container = $container;
    }

    // Instance creation

    /**
     *
     * @param array $data
     * @return static
     */
    public static function instance(array $data = [])
    {
        $instance = new static();

        if (!empty($data)) {
            $instance->setAttributes($data);
        }

        return $instance;
    }

    /**
     *
     * @param array $data
     * @param \Nette\Database\Table\Selection $table
     * @return static
     */
    public static function fromSelection(array $data = [], \Nette\Database\Table\Selection $table = NULL)
    {
        $instance = self::instance();

        $instance->writePrivateProperty($instance, 'data', $data);
        $instance->writePrivateProperty($instance, 'table', $table);

        return $instance;
    }

    /**
     *
     * @param array $data
     * @return self
     */
    public static function create(array $data)
    {
        $instance = self::instance($data);
        $instance->save();

        return $instance;
    }

    /**
     * @param type $id
     * @return static
     */
    public static function find($id)
    {
        if (is_array($id))
            return self::where(static::identifier(), $id);

        return self::createCollection()->get($id);
    }

    /**
     *
     * @param type $id
     * @return static
     * @throws \Nette\InvalidArgumentException
     */
    public static function findOrFail($id)
    {
        $item = self::find($id);

        // TODO: exception
        if (!$item)
            throw new \Nette\InvalidArgumentException('Object not found');

        return $item;
    }

    /**
     * @param $id
     * @return static
     */
    public static function findOrDefault($id, array $data = [])
    {
        $item = static::find($id);

        if (!$item)
            return static::instance($data);

        return $item;
    }

    // Collection creation

    /**
     *
     * @return Collection
     */
    public static function all()
    {
        return self::createCollection();
    }

    public static function where($condition, ...$parameters)
    {
        return call_user_func_array([self::all(), 'where'], func_get_args());
    }

    /**
     *
     * @return Collection
     * @throws \Nette\NotSupportedException
     */
    public static function withTrashed()
    {
        if (!self::config()->softDeletes())
            throw new \Nette\NotSupportedException('Model does not support soft deletes');

        return self::createCollection(TRUE);
    }

    /**
     *
     * @return Collection
     * @throws \Nette\NotSupportedException
     */
    public static function onlyTrashed()
    {
        if (!self::config()->softDeletes())
            throw new \Nette\NotSupportedException('Model does not support soft deletes');

        return self::withTrashed()->where(self::config()->softDeleteColumn() . ' NOT ?', NULL);
    }

    public static function search(Selection $collection, $value, array $args = [])
    {
        $columns = self::config()->searchedColumns();

        if (empty($columns))
            return $collection;

        $query = [];
        $params = [];
        foreach ($columns as $column) {
            $query[] = "{$column} LIKE ?";
            $params[] = "%{$value}%";
        }

        if (!empty($args)) {
            call_user_func_array('array_push', array_merge([&$query], array_keys($args)));
            call_user_func_array('array_push', array_merge([&$params], array_values($args)));
        }

        return $collection->whereOr(array_combine($query, $params));
    }

    public static function identifier()
    {
        return self::createCollection()->getPrimary();
    }

    public static function columns()
    {
        $columns = static::$context->getConnection()->getSupplementalDriver()->getColumns(static::config()->table());

        return array_map(function($column) {
            return $column['name'];
        }, $columns);
    }

    protected static function config()
    {
        return new static();
    }

    protected static function createCollection($includeDeleted = FALSE)
    {
        $config = self::config();

        $table = self::$context->table($config->table());
        $collection = new Collection($table, get_class($config), $config->plugins());

        if (!$includeDeleted && $config->softDeletes())
            $collection->where($config->softDeleteColumn(), NULL);

        $config->callPlugins('filterResults', [ &$collection ]);

        return $collection;
    }





    ////////////////////////////////////////////////////////////////////////////
    /// CONFIG METHODS                                                       ///
    ////////////////////////////////////////////////////////////////////////////

    protected abstract function table();

    protected function setup()
    {

    }

    protected function plugins()
    {
        return [
            new Plugins\TimestampPlugin()
        ];
    }

    protected function softDeletes()
    {
        return TRUE;
    }

    protected function softDeleteColumn()
    {
        return 'deleted_at';
    }

    protected function hiddenAttributes()
    {
        return [];
    }

    protected function attributeTypes()
    {
        return [];
    }

    protected function serializationDateFormat()
    {
        return 'c';
    }

    protected function searchedColumns()
    {
        return [];
    }










    ////////////////////////////////////////////////////////////////////////////
    /// PUBLIC INSTANCE METHODS                                              ///
    ////////////////////////////////////////////////////////////////////////////

    public function fill(array $data)
    {
        $this->setAttributes($data);

        return $this;
    }

    public function isSaved()
    {
        return $this->getTable() && empty($this->attributes);
    }

    public function isNew()
    {
        return !$this->getTable();
    }

    public function isTrashed()
    {
        if (!$this->softDeletes())
            return FALSE;

        return $this->{$this->softDeleteColumn()} !== NULL;
    }

    public function save()
    {
        if (empty($this->attributes))
            return;

        if (!$this->getTable()) {
            $this->insert();

        } else {
            $this->updateInstance();
        }

        $this->attributes = [];

        return $this;
    }

    public function update($data)
    {
        $this->setAttributes($data);

        return $this->updateInstance();
    }

    public function delete()
    {
        if ($this->softDeletes()) {
            $args = new DeleteArgs([
                'id'        => $this->getPrimary(),
                'instance'  => $this,
                'soft'      => true
            ]);

            $this->raise('deleting', $args);
            $this->callPlugins('beforeDelete', [ $this->getPrimary(), $this, true ]);

            $this->update([
                $this->softDeleteColumn() => new \Nette\Utils\DateTime()
            ]);

            $this->callPlugins('afterDelete', [ $this, true ]);
            $this->raise('deleted',  $args);

        } else  {
            $this->forceDelete();
        }

        return $this;
    }

    public function forceDelete()
    {
        $args = new DeleteArgs([
            'id'        => $this->getPrimary(),
            'instance'  => $this,
            'soft'      => false
        ]);

        $this->raise('deleting', $args);
        $this->callPlugins('beforeDelete', [ $this->getPrimary(), $this, false ]);

        parent::delete();

        $this->callPlugins('afterDelete', [ $this, false ]);
        $this->raise('deleted', $args);

        return $this;
    }

    public function restore()
    {
        if (!$this->softDeletes())
            throw new \Nette\NotSupportedException('Model does not support soft deletes');

        $args = new RestoreArgs([
            'id'        => $this->getPrimary(),
            'instance'  => $this,
        ]);

        $this->raise('restoring', $args);

        $this->update([
            $this->softDeleteColumn() => NULL
        ]);

        $this->raise('restored', $args);

        return $this;
    }

    public function duplicate(array $values = [], array $except = [])
    {
        $except = array_merge([
            $this->softDeleteColumn(),
            'created_at',
            'updated_at'
        ], (array) $this->getTable()->getPrimary(FALSE), $except);

        $data = $this->toNativeArray();

        foreach ($except as $property) {
            if (isset($data[$property]))
                unset($data[$property]);
        }

        return static::create(array_merge($data, $values));
    }

    public function toArray()
    {
        $values = $this->toNativeArray();

        foreach ($values as $key => $value) {
            $values[$key] = $this->readField($key, $value);

            if ($value instanceof \DateTime)
                $values[$key] = $value->format($this->serializationDateFormat());

            if (in_array($key, $this->hiddenAttributes()))
                unset($values[$key]);

            if ($value instanceof \Varhall\Utilino\ISerializable) {
                $values[$key] = $value->toArray();
            }
        }

        return $values;
    }

    public function toJson()
    {
        return \Nette\Utils\Json::encode($this->toArray());
    }




    ////////////////////////////////////////////////////////////////////////////
    /// PRIVATE & PROTECTED METHODS                                          ///
    ////////////////////////////////////////////////////////////////////////////

    protected function insert()
    {
        // raise events
        $a1 = new InsertArgs([ 'data' => $this->attributes ]);

        $this->raise('creating', $a1);
        $this->callPlugins('beforeInsert', [ &$this->attributes ]);

        // insert
        $model = self::createCollection()->insert($this->attributes);

        $this->writePrivateProperty($this, 'data', $this->readPrivateProperty($model, 'data'));
        $this->writePrivateProperty($this, 'table', $this->readPrivateProperty($model, 'table'));

        // raise events
        $a2 = new InsertArgs([
            'data'      => $this->attributes,
            'instance'  => $model,
            'id'        => $model->getPrimary()
        ]);

        $this->callPlugins('afterInsert', [ $model ]);
        $this->raise('created', $a2);
    }

    protected function updateInstance()
    {
        // prcompute data
        $originals = parent::toArray();
        $diff = array_filter($this->attributes, function($value, $key) use ($originals) {
            return $value != $originals[$key];
        }, ARRAY_FILTER_USE_BOTH);

        // raise events
        $args = new UpdateArgs([
            'data'      => $this->toNativeArray(),
            'instance'  => $this,
            'id'        => $this->getPrimary(),
            'diff'      => $diff
        ]);

        $this->raise('updating', $args);
        $this->callPlugins('beforeUpdate', [ $this->getPrimary(), &$this->attributes, $diff ]);

        // update
        $result = parent::update($this->attributes);

        // raise events
        $this->callPlugins('afterUpdate', [ $this->getPrimary(), $this, $diff ]);
        $this->raise('updated', $args);

        return $result;
    }

    protected function hasMany($class, $throughColumn = NULL)
    {
        $config = new $class();

        $related = $this->related($config->table(), $throughColumn);

        $collection = new GroupedCollection($related, get_class($config), $config->plugins());

        if ($config->softDeletes())
            $collection->where($config->softDeleteColumn(), NULL);

        return $collection;
    }

    protected function belongsTo($class, $throughColumn = NULL)
    {
        $instance = new $class();

        if (!$this->getTable())
            return NULL;

        $ref = $this->ref($instance->table(), $throughColumn);

        if (!$ref)
            return NULL;

        $this->writePrivateProperty($instance, 'data', $this->readPrivateProperty($ref, 'data'));
        $this->writePrivateProperty($instance, 'table', $this->readPrivateProperty($ref, 'table'));

        return $instance;
    }

    protected function belongsToMany($class, $intermediateTable, $foreignColumn, $referenceColumn)
    {
        $config = new $class();

        $intermediate = $this->related($intermediateTable, $foreignColumn);
        $collection = new ManyToManyCollection($intermediate, $class, $intermediateTable, $config->table(), $foreignColumn, $referenceColumn, $this->getPrimary());

        //$intermediate = self::$context->table($intermediateTable)->where($foreignColumn, $this->getPrimary())->select($referenceColumn);
        //$collection = $class::all()->wherePrimary($intermediate);

        //$intermediate = self::$context->table($intermediateTable)->where($foreignColumn, $this->getPrimary())->select($referenceColumn);
        //$collection = (new ManyToManyCollection($class::all(), $class, $intermediateTable, $config->table(), $foreignColumn, $referenceColumn, $this->getPrimary()))->wherePrimary($intermediate);

        if ($config->softDeletes())
            $collection->where($config->softDeleteColumn(), NULL);

        return $collection;
    }

    private function readPrivateProperty($object, $property)
    {
        if (!($object instanceof \Nette\Database\Table\ActiveRow))
            return;

        $class = new \ReflectionClass(\Nette\Database\Table\ActiveRow::class);
        $prop = $class->getProperty($property);
        $prop->setAccessible(true);

        return $prop->getValue($object);
    }

    private function writePrivateProperty($object, $property, $value)
    {
        if (!($object instanceof \Nette\Database\Table\ActiveRow))
            return;

        $class = new \ReflectionClass(\Nette\Database\Table\ActiveRow::class);
        $prop = $class->getProperty($property);
        $prop->setAccessible(true);

        $prop->setValue($object, $value);
    }

    /**
     * Calls method on each enabled plugin with given args
     *
     * @param string $function Function name
     * @param array $args Array of arguments which are passed into a function
     */
    protected function callPlugins($function, array $args)
    {
        foreach ($this->plugins() as $plugin) {
            $plugin->setModel($this);
            $plugin->setContext(static::$context);
            $plugin->setContainer(static::$container);

            $callback = [$plugin, $function];

            if (is_callable($callback))
                call_user_func_array($callback, $args);
        }
    }

    protected function getAttribute($key)
    {
        // get unsaved value if set
        if (isset($this->attributes[$key]))
            return $this->readField($key, $this->attributes[$key]);

        // call parent getter
        $value = NULL;

        try {
            if ($this->getTable())
                $value = parent::__get($key);

        } catch (\Nette\MemberAccessException $ex) {
            $this->accessColumn(NULL);
            $value = parent::__get($key);
        }

        $value = $this->readField($key, $value);
        return $value;
    }

    protected function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    protected function setAttributes($data)
    {
        foreach ($data as $key => $value) {
            $this->__set($key, $value);
        }
    }

    protected function toNativeArray()
    {
        $values = array_merge($this->isSaved() ? parent::toArray() : [], $this->attributes);

        if (!$values || !is_array($values)) {
            throw new \Nette\InvalidStateException('Unable convert row to array');
        }

        return $values;
    }

    protected function readField($key, &$value)
    {
        foreach ($this->plugins() as $plugin) {
            $plugin->setModel($this);
            $plugin->setContext(static::$context);
            $plugin->setContainer(static::$container);

            $value = $plugin->readField($key, $value);
        }

        if (array_key_exists($key, $this->attributeTypes()))
            $value = $this->castField($key, $value);

        return $value;
    }

    protected function toCamelCase($input)
    {
        $case = implode('',
            array_map(
                function($item) { return ucfirst(strtolower($item)); },
                explode('_', $input)
            )
        );

        return lcfirst($case);
    }

    protected function fromCamelCase($input)
    {
        $input = ucfirst($input);

        preg_match_all('/([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)/', $input, $matches);

        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return strtolower(implode('_', $ret));
    }

    protected function on($event, callable $callback)
    {
        $this->events[$event][] = $callback;
    }

    protected function raise($event, ...$args)
    {
        if (!isset($this->events[$event]))
            return;

        foreach ($this->events[$event] as $handler) {
            if (!is_callable($handler))
                continue;

            call_user_func_array($handler, $args);
        }
    }

    private function castField($field, $value)
    {
        $type = $this->attributeTypes()[$field];

        switch ($type) {
            case 'bool':
            case 'boolean':
                return boolval($value);

            case 'int':
            case 'integer':
                return intval($value);

            case 'double':
                return doubleval($value);

            case 'float':
            case 'real':
                return floatval($value);

            case 'string':
                return strval($value);
        }

        return $value;
    }
}
