<?php

namespace Varhall\Dbino\Events;

class UpdateArgs extends EventArgs
{
    public $data = null;

    public $diff = [];
}