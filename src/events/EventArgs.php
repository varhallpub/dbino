<?php

namespace Varhall\Dbino\Events;


abstract class EventArgs
{
    public $id = null;

    public $instance = null;

    public function __construct($args = [])
    {
        foreach ($args as $name => $value) {
            if (property_exists(static::class, $name))
                $this->$name = $value;
        }
    }
}