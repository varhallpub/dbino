<?php

namespace Varhall\Dbino\Events;

class InsertArgs extends EventArgs
{
    public $data = null;
}