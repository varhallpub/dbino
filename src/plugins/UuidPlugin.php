<?php

namespace Varhall\Dbino\Plugins;

class UuidPlugin extends ModelPlugin
{
    protected $columns = [];

    public function __construct($columns)
    {
        $this->columns = (array) $columns;
    }

    public function beforeInsert(array &$data)
    {
        foreach ($this->columns as $column) {
            if (empty($data[$column]))
                $data[$column] = $this->guid();
        }
    }

    protected function guid()
    {
        if (function_exists('com_create_guid') === true) {
            return strtolower(trim(com_create_guid(), '{}'));
        }

        $guid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        return strtolower($guid);
    }
}