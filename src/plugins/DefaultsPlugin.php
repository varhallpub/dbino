<?php

namespace Varhall\Dbino\Plugins;

/**
 * Model extension plugin. This plugin fills model with defaults when the values are inserted or fills with defaults
 * when the values are read and unset.
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class DefaultsPlugin extends ModelPlugin
{
    /**
     * @var array
     */
    private $defaults = [];
    
    /**
     * Pole sloupcu, ktere urcuji podmnozinu radku v tabulce<br>
     * Pole je definovane jako asociativni pole ve tvaru [ sloupec => hodnota ]<br>
     * <br>
     * <b>Priklad:</b><br>
     * [ customer_id => 1 ]<br>
     * 
     * @param array $defaults
     */
    public function __construct(array $defaults = [])
    {
        $this->defaults = array_merge($this->defaults, $defaults);
    }
    
    
    
     /// PLUGIN METHODS
    
    public function readField($field, $value)
    {
        if (empty($value) && isset($this->defaults[$field]))
            $value = $this->defaults[$field];

        return $value;
    }

    public function beforeInsert(array &$data)
    {
        foreach ($this->defaults as $key => $value) {
            if (!isset($data[$key]))
                $data[$key] = $value;
        }
        
        return $data;
    }
}
