<?php

namespace Varhall\Dbino\Plugins;

use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Varhall\Dbino\Plugins\Json\JsonObject;

/**
 * Model extension plugin. This plugin transforms a database TEXT column to JSON value.
 * When the column is read, value is automatically transformed to object and back.
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class JsonPlugin extends ModelPlugin
{
    const OPTIONS_SERIALIZERS   = 'serializers';
    const OPTIONS_NULLABLE      = 'nullable';
    const OPTIONS_PRIMITIVE     = 'primitve';

    protected $fields = [];

    protected $serializers = [];

    protected $nullable = [];

    protected $primitive = [];

    public function __construct($fields = [], $options = [])
    {
        $fields = $this->createDefaults((array) $fields);

        if (isset($options[self::OPTIONS_SERIALIZERS]))
            $this->serializers = (array) $options[self::OPTIONS_SERIALIZERS];

        if (isset($options[self::OPTIONS_NULLABLE])) {
            $this->nullable = ($options[self::OPTIONS_NULLABLE] === true)
                ? array_keys($fields)
                : (array) $options[self::OPTIONS_NULLABLE];
        }

        if (isset($options[self::OPTIONS_PRIMITIVE])) {
            $this->primitive = ($options[self::OPTIONS_PRIMITIVE] === true)
                ? array_keys($fields)
                : (array) $options[self::OPTIONS_PRIMITIVE];
        }

        foreach ($fields as $field => $defaults) {
            $this->addJsonField($field, $defaults);
        }
    }

    public function addJsonField($field, array $defaults = [])
    {
        $this->fields[$field] = $defaults;
    }


    /// PLUGIN METHODS

    public function readField($field, $value)
    {
        if (!isset($this->fields[$field]))
            return $value;

        if ($this->isNullable($field) && $value === null)
            return $value;

        try {
            if (is_string($value) && !empty($value))
                $value = \Nette\Utils\Json::decode($value, Json::FORCE_ARRAY);

            else if ($value instanceof JsonObject)
                $value = $value->toArray();

            return new JsonObject(array_merge($this->fields[$field], (array)$value));

        } catch (JsonException $ex) {
            if ($this->allowsPrimitive($field))
                return $value;

            throw $ex;
        }
    }

    public function beforeInsert(array &$data)
    {
        $this->encodeFields($data, TRUE);

        return $data;
    }

    public function beforeUpdate($id, array &$data, array $diff)
    {
        $this->encodeFields($data);
    }


    /// PRIVATE & PROTECTED METHODS

    protected function createDefaults(array $jsonFields)
    {
        $result = [];

        foreach ($jsonFields as $field => $defaults) {
            // if field is given in non-associative array, transform to associative with empty defaults
            if (is_int($field)) {
                $field = $defaults;
                $defaults = [];
            }

            $result[$field] = $defaults;
        }

        return $result;
    }

    protected function encodeFields(array &$data, $initialize = false)
    {
        foreach ($this->fields as $field => $defaults) {
            if ($initialize && !isset($data[$field]))
                $data[$field] = $this->isNullable($field) ? null : [];

            if (!isset($data[$field]))
                continue;

            if (isset($this->serializers[$field]) && is_callable($this->serializers[$field]))
                $data[$field] = call_user_func($this->serializers[$field], $data[$field]);

            if (is_array($data[$field]) || is_object($data[$field]))
                $data[$field] = Json::encode($data[$field]);
        }
    }

    protected function isNullable($field)
    {
        return in_array($field, $this->nullable);
    }

    protected function allowsPrimitive($field)
    {
        return in_array($field, $this->primitive);
    }
}
