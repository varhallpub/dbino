<?php

namespace Varhall\Dbino\Plugins;

use Nette\Database\Table\ActiveRow;

/**
 * Single usage plugin which allows to pass anonymous callback functions
 */
class EventsPlugin extends ModelPlugin
{
    const BEFORE_INSERT = 'beforeInsert';
    const BEFORE_UPDATE = 'beforeUpdate';
    const BEFORE_DELETE = 'beforeDelete';
    const BEFORE_SAVE   = 'beforeSave';
    const BEFORE_ALL    = 'beforeAll';
    const AFTER_INSERT  = 'afterInsert';
    const AFTER_UPDATE  = 'afterUpdate';
    const AFTER_DELETE  = 'afterDelete';
    const AFTER_SAVE    = 'afterSave';
    const AFTER_ALL     = 'afterAll';

    private $events = [];


    public function __construct($events)
    {
        $this->events = $events;
    }


    /// PLUGIN METHODS

    public function beforeInsert(array &$data)
    {
        $this->raiseEvent(self::BEFORE_INSERT, [ $data ]);
        $this->raiseEvent(self::BEFORE_SAVE, [ $data ]);
        $this->raiseEvent(self::BEFORE_ALL, [ $data ]);
    }

    public function afterInsert(ActiveRow $item)
    {
        $this->raiseEvent(self::AFTER_INSERT, [ $item ]);
        $this->raiseEvent(self::AFTER_SAVE, [ $item ]);
        $this->raiseEvent(self::AFTER_ALL, [ $item ]);
    }

    public function beforeUpdate($id, array &$data, array $diff)
    {
        $this->raiseEvent(self::BEFORE_UPDATE, [ $id, $data, $diff ]);
        $this->raiseEvent(self::BEFORE_SAVE, [ $data, $diff ]);
        $this->raiseEvent(self::BEFORE_ALL, [ $data ]);
    }

    public function afterUpdate($id, ActiveRow $item, array $diff)
    {
        $this->raiseEvent(self::AFTER_UPDATE, [ $item, $diff ]);
        $this->raiseEvent(self::AFTER_SAVE, [ $item ]);
        $this->raiseEvent(self::AFTER_ALL, [ $item ]);
    }

    public function beforeDelete($id, ActiveRow $item, $soft)
    {
        $this->raiseEvent(self::BEFORE_DELETE, [ $id, $item ]);
        $this->raiseEvent(self::BEFORE_ALL, [ [] ]);
    }

    public function afterDelete(ActiveRow $item, $soft)
    {
        $this->raiseEvent(self::AFTER_DELETE, [ $item ]);
        $this->raiseEvent(self::AFTER_ALL, [ $item ]);
    }



    /// PRIVATE & PROTECTED METHODS

    protected function raiseEvent($event, array $data)
    {
        if (!isset($this->events[$event]) || empty($this->events[$event]))
            return;

        if (!is_callable($this->events[$event]))
            return;

        call_user_func_array($this->events[$event], $data);
    }
}
