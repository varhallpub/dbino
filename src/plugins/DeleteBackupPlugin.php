<?php

namespace Varhall\Dbino\Plugins;


class DeleteBackupPlugin extends ModelPlugin {

    protected $definitions = [];

    public $deletedColumn = 'deleted_at';

    public function __construct(array $definitions)
    {
        $this->definitions = $definitions;
    }

    public function beforeUpdate($id, array &$data, array $diff)
    {
        if (!isset($data[$this->deletedColumn]) || $data[$this->deletedColumn] === NULL)
            return;

        foreach ($this->definitions as $column => $callback) {
            if (!is_callable($callback))
                continue;

            $data[$column] = call_user_func($callback, $data);
        }
    }

}