<?php

namespace Varhall\Dbino\Plugins;

use Nette\Caching\Cache;
use Nette\Caching\Storages\MemoryStorage;

trait TreeNode
{
    private static $cache = NULL;

    /// STATIC METHODS

    public static function all()
    {
        return static::createCollection()->order(static::config()->treeColumns()->left);
    }

    protected static function paths($column, $separator = ',')
    {
        $config = static::config();

        $table = $config->table();
        $left = $config->treeColumns()->left;
        $right = $config->treeColumns()->right;

        return static::$context
            ->query("SELECT t0.id AS id, 
                                (SELECT GROUP_CONCAT(t2.{$column} ORDER BY t2.{$left} SEPARATOR '{$separator}')
                                    FROM {$table} t2 
                                    WHERE t2.{$left} <= t0.{$left} AND t2.{$right} >= t0.{$right} AND t2.deleted_at IS NULL
                                ) AS path,
                                (SELECT COUNT(t2.id)
                                    FROM {$table} t2 
                                    WHERE t2.{$left} < t0.{$left} AND t2.{$right} > t0.{$right} AND t2.deleted_at IS NULL
                                ) AS depth
                            FROM {$table} t0
                            WHERE t0.deleted_at IS NULL
                            GROUP BY t0.id;");
    }

    public static function orphans()
    {
        $data = static::all()->asArray();

        $items = static::traverse($data);
        return static::where('id NOT', $items);
    }

    private static function traverse($data, $left = 0, $right = null)
    {
        $tree = [];

        foreach ($data as $item) {
            $l = $item->{$item->treeColumns()->left};
            $r = $item->{$item->treeColumns()->right};

            if ($l === $left + 1 && (is_null($right) || $r < $right)) {
                $tree = array_merge($tree, [$item->id], static::traverse(array_filter($data, function ($x) use ($l, $r) {
                    return $x->{$x->treeColumns()->left} > $l && $x->{$x->treeColumns()->right} < $r;
                }), $l, $r));

                $left = $r;
            }
        }

        return $tree;
    }


    /// METHODS

    public function moveToEnd()
    {
        $max = static::all()->max($this->treeColumns()->left) ?? 0;

        $this->{$this->treeColumns()->left} = $max + 1;
        $this->{$this->treeColumns()->right} = $max + 2;
        $this->{$this->treeColumns()->parent} = null;

        return $this->save();
    }


    /// DYNAMIC PROPERTIES

    public function parent_node()
    {
        return $this->belongsTo(static::class, $this->treeColumns()->parent);
    }

    public function root_node()
    {
        return $this->ancestors(true)->first();
    }

    public function children()
    {
        return $this->hasMany(static::class, $this->treeColumns()->parent);
    }

    public function ancestors($includeSelf = FALSE)
    {
        $left = $this->treeColumns()->left;
        $right = $this->treeColumns()->right;

        return static::all()
            ->where('lpos <' . ($includeSelf ? '=' : '') . ' ?', $this->{$left})
            ->where('rpos >' . ($includeSelf ? '=' : '') . ' ?', $this->{$right});
    }

    public function descendants($includeSelf = FALSE)
    {
        $left = $this->treeColumns()->left;
        $right = $this->treeColumns()->right;

        return static::all()
            ->where('lpos >' . ($includeSelf ? '=' : '') . ' ?', $this->{$left})
            ->where('rpos <' . ($includeSelf ? '=' : '') . ' ?', $this->{$right});
    }

    public function path($separator = ' > ')
    {
        if (!static::$cache)
            static::$cache = new Cache(new MemoryStorage());

        $result = static::$cache->load('shop-categories-paths', function() use ($separator) {
            return static::paths('name', $separator)->fetchPairs('id');
        });

        return $result[$this->getPrimary()]->path;
    }

    /// CONFIGURATION

    public static function treeColumnsConfig()
    {
        return static::config()->treeColumns();
    }

    protected function treeColumns()
    {
        return (object) [
            'left'      => 'lpos',
            'right'     => 'rpos',
            'parent'    => 'parent'
        ];
    }
}