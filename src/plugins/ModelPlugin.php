<?php

namespace Varhall\Dbino\Plugins;

use Nette\Database\Context;
use Nette\Database\Table\ActiveRow;
use Nette\DI\Container;
use Varhall\Dbino\Model;

/**
 * Base class of model extension
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
abstract class ModelPlugin
{
    /**
     * @var Model
     */
    protected $model        = NULL;

    /**
     * @var Context
     */
    protected $context      = NULL;

    /**
     * @var Container
     */
    protected $container    = NULL;


    public function setModel($model)
    {
        $this->model = $model;
    }

    public function setContext($context)
    {
        $this->context = $context;
    }

    public function setContainer($container)
    {
        $this->container = $container;
    }



    //////////////////////////////////////////// PLUGIN METHODS ////////////////////////////////////////////////////////

    public function filterResults(\Nette\Database\Table\Selection &$table)
    {
        
    }
    
    public function readField($field, $value)
    {
        return $value;
    }
    
    public function beforeInsert(array &$data)
    {
        
    }

    public function afterInsert(ActiveRow $item)
    {

    }
    
    public function beforeUpdate($id, array &$data, array $diff)
    {
        
    }

    public function afterUpdate($id, ActiveRow $item, array $diff)
    {

    }
    
    public function beforeDelete($id, ActiveRow $item, $soft)
    {
        
    }

    public function afterDelete(ActiveRow $item, $soft)
    {

    }
}
