<?php

namespace Varhall\Dbino\Plugins;

/**
 * Model extension plugin. This plugin automatically manages object creation and modification timestamps.
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class TimestampPlugin extends ModelPlugin
{
    const FIELD_CREATED = 'created_at';
    
    const FIELD_UPDATED = 'updated_at';
    
    /**
     * @var string
     */
    private $_createdAt = self::FIELD_CREATED;
    
    /**
     * @var string
     */
    private $_updatedAt = self::FIELD_UPDATED;
    
    
    /**
     * Pole sloupcu, ktere urcuji podmnozinu radku v tabulce<br>
     * Pole je definovane jako asociativni pole ve tvaru [ sloupec => hodnota ]<br>
     * <br>
     * <b>Priklad:</b><br>
     * [ customer_id => 1 ]<br>
     * 
     * @param array $scopeFields
     */
    public function __construct($createdAt = self::FIELD_CREATED, $updatedAt = self::FIELD_UPDATED)
    {
        $this->_createdAt = $createdAt;
        $this->_updatedAt = $updatedAt;
    }

    
     /// PLUGIN METHODS
    
    public function beforeInsert(array &$data)
    {
        $data[$this->_createdAt] = new \Nette\Utils\DateTime();
        $data[$this->_updatedAt] = NULL;
        
        return $data;
    }

    public function beforeUpdate($id, array &$data, array $diff)
    {
        if (isset($data[$this->_createdAt]))
            unset($data[$this->_createdAt]);

        $data[$this->_updatedAt] = new \Nette\Utils\DateTime();
    }
    
    /// PRIVATE & PROTECTED METHODS
    
}
