<?php

namespace Varhall\Dbino\Plugins;

class TreePlugin extends ModelPlugin
{
    private $left = NULL;

    private $right = NULL;

    private $current = NULL;

    public function __construct($left, $right, $current)
    {
        $this->left = $left;
        $this->right = $right;
        $this->current = $current;
    }

    public function beforeInsert(array &$data)
    {
        if (!isset($data[$this->left]) && !isset($data[$this->right])) {
            $max = $this->maxIndex();

            $data[$this->left] = $max + 1;
            $data[$this->right] = $max + 2;
        }
    }

    private function maxIndex()
    {
        $class = get_class($this->current);

        $max = $class::all()->max($this->right);

        return $max ? $max : 0;
    }
}