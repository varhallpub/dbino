<?php

namespace Varhall\Dbino;
use Nette\Database\Table\Selection;
use Varhall\Utilino\Collections\ArrayCollection;
use Varhall\Utilino\Collections\ICollection;

/**
 * Nette Database collection extension
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
trait CollectionTrait
{
    /**
     * @var string
     */
    protected $model = '';

    /**
     * @var array
     */
    protected $plugins = [];


    public function __call($name, $arguments)
    {
        if (preg_match('/^where(.*)If$/i', $name))
            return call_user_func_array([ $this, 'whereIf' ], array_merge([$name], $arguments));

        if (preg_match('/^where/i', $name) && method_exists($this->model, $name))
            return call_user_func_array([ $this->model, $name ], array_merge([$this], $arguments));
    }

    protected function createRow(array $row)
    {
        return call_user_func([$this->model, 'fromSelection'], $row, $this);
    }

    public function columns()
    {
        return call_user_func([$this->model, 'columns']);
    }

    protected function whereIf($method, $condition, ...$params)
    {
        if (is_callable($condition) && !call_user_func($condition))
            return $this;

        if (!$condition)
            return $this;

        $method = preg_replace('/If$/i', '', $method);

        return call_user_func_array([$this, $method], $params);
    }

    public function insert($data)
    {
        $row = parent::insert($data);

        if ($row instanceof \Nette\Database\Table\Selection) {
            return new Collection($row, $this->model, $this->plugins);

        } else if ($row instanceof \Nette\Database\Table\ActiveRow) {
            $table = $this->readPrivateProperty($row, 'table');
            $data  = $this->readPrivateProperty($row, 'data');

            return call_user_func([$this->model, 'fromSelection'], $data, $table);
        }

        return $row;
    }

    protected function decorate(\Nette\Database\Table\ActiveRow $row)
    {
        foreach ($this->plugins as $plugin) {
            $row = $plugin->decorateRow($row);
        }

        return $row;
    }

    private function readPrivateProperty($object, $property)
    {
        $value = \Closure::bind(function() use ($property) {
            return $this->$property;
        }, $object, $object)->__invoke();

        return $value;
    }

    public function refresh()
    {
        // TODO: possibly reload all columns
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// ICollection interface
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Executes function for each item in collection
     *
     * @param callable $func args: $item
     * @return ICollection
     */
    public function each(callable $func)
    {
        $index = 0;

        foreach ($this->fetchAll() as $key => $value) {
            if (call_user_func_array($func, [ $value, $key, $index++ ]) === FALSE)
                break;
        }

        return $this;
    }

    /**
     * Returns true if every item in collection matches given function
     *
     * @param callable $func args: $item
     * @return boolean
     */
    public function every(callable $func)
    {
        return $this->toArrayCollection()->every($func);
    }

    /**
     * Return true if any of items in collection matches given function
     *
     * @param callable $func
     * @return boolean
     */
    public function any(callable $func)
    {
        return $this->toArrayCollection()->any($func);
    }

    /**
     * Returns new collection where each item matches given function
     *
     * @param callable $func args: $item
     * @return ICollection
     */
    public function filter(callable $func)
    {
        return $this->toArrayCollection()->filter($func);
    }

    /**
     * Returns new colletion where each key is in given array or matches given function
     *
     * @param $keys array|callable
     * @return ICollection
     */
    public function filterKeys($keys)
    {
        return $this->toArrayCollection()->filterKeys($keys);
    }

    /**
     * Returns first item which matches function if given
     *
     * @param callable|NULL $func args: $item
     * @return mixed
     */
    public function first(callable $func = NULL)
    {
        if ($this->isEmpty())
            return NULL;

        return $func ? $this->filter($func)->first() : $this->fetch();
    }

    /**
     * @return Reduces level of collection
     */
    public function flatten()
    {
        return $this->toArrayCollection()->flatten();
    }

    /**
     * True if collection is empty
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->count() === 0;
    }

    /**
     * Collection of keys
     *
     * @return ICollection
     */
    public function keys()
    {
        return new ArrayCollection(array_keys($this->fetchPairs()));
    }

    /**
     * Returns last item which matches function if given
     *
     * @param callable|NULL $func args: $item
     * @return mixed
     */
    public function last(callable $func = NULL)
    {
        return $this->toArrayCollection()->last($func);
    }

    /**
     * Transforms each item using given function
     *
     * @param callable $func args: $item
     * @return ICollection
     */
    public function map(callable $func)
    {
        return $this->toArrayCollection()->map($func);
    }

    /**
     * Merge array or collection with current collection
     *
     * @param ICollection|array $collection
     * @return ICollection
     */
    public function merge($collection)
    {
        $array = ($collection instanceof Selection)
            ? $collection->fetchAll()
            : (
            ($collection instanceof ICollection)
                ? $collection->asArray()
                : $collection
            );

        return new ArrayCollection(array_merge($this->asArray(), $array));
    }

    /**
     * Fills current collection to required length with default value
     *
     * @param $size
     * @param $value
     * @return mixed
     */
    public function pad($size, $value)
    {
        return $this->toArrayCollection()->pad($size, $value);
    }

    /**
     * Passes current collection into given function and returns the function value
     *
     * @param callable $func args: $this
     * @return mixed
     */
    public function pipe(callable $func)
    {
        return call_user_func_array($func, [ $this ]);
    }

    /**
     * Removes and returns last item from collection
     *
     * @return mixed
     */
    public function pop()
    {
        return $this->toArrayCollection()->pop();
    }

    /**
     * Inserts value at the begining of the collection
     *
     * @param $value
     * @return mixed
     */
    public function prepend($value)
    {
        return $this->toArrayCollection()->prepend($value);
    }

    /**
     * Adds the value in the and of the collection
     *
     * @param $value
     * @return mixed
     */
    public function push($value)
    {
        return $this->toArrayCollection()->push($value);
    }

    /**
     * Accumulates value through all elements from the start to end
     *
     * @param callable $func
     * @param mixed|null $initial
     * @return mixed
     */
    public function reduce(callable $func, $initial = NULL)
    {
        return $this->toArrayCollection()->reduce($func, $initial);
    }

    public function reverse()
    {
        return $this->toArrayCollection()->reverse();
    }

    /**
     * Removes first value from the collection
     *
     * @return mixed
     */
    public function shift()
    {
        return $this->toArrayCollection()->shift();
    }

    /**
     * Sorts collection using given compare function
     *
     * @param callable $func args: $a, $b
     * @return ICollection
     */
    public function sort(callable $func)
    {
        return $this->toArrayCollection()->sort($func);
    }

    /**
     * Returns collection of values
     *
     * @return ICollection
     */
    public function values()
    {
        return new ArrayCollection(array_values($this->fetchPairs()));
    }

    public function search($value, callable $func = NULL)
    {
        $func = $func !== NULL ? $func : [$this->model, 'search'];

        return call_user_func($func, $this, $value);
    }

    public function asArray()
    {
        $this->refresh();
        return array_values($this->fetchAll());
    }

    public function toArray()
    {
        $result = [];

        foreach ($this as $item) {
            $result[] = $item->toArray();
        }

        return $result;
    }

    public function toJson()
    {
        return \Nette\Utils\Json::encode($this->toArray());
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    protected function toArrayCollection()
    {
        return new ArrayCollection($this->asArray());
    }

}
