<?php

namespace Varhall\Dbino\DI;

/**
 * Description of DbinoExtension
 *
 * @author fero
 */
class DbinoExtension extends \Nette\DI\CompilerExtension
{
    public function afterCompile(\Nette\PhpGenerator\ClassType $class)
    {
        parent::afterCompile($class);
        
        // metoda initialize
        $initialize = $class->getMethod('initialize');

        $initialize->addBody('\Varhall\Dbino\Model::setContext($this->getByType(?));', ['\Nette\Database\Context']);
        $initialize->addBody('\Varhall\Dbino\Model::setContainer($this->getByType(?));', ['\Nette\DI\Container']);
    }
}
