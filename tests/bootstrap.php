<?php

require_once __DIR__ . '/../vendor/autoload.php';

\Tester\Environment::setup();
date_default_timezone_set('UTC');

function test(\Closure $function)
{
    $function();
}

\Tester\Environment::lock('dbino', __DIR__ . '/temp');

$loader = new Nette\Loaders\RobotLoader();
$loader->addDirectory(__DIR__);
$loader->addDirectory(__DIR__ . '/../src');

$loader->setTempDirectory(__DIR__ . '/temp');
$loader->register();
