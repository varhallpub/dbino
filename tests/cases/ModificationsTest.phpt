<?php

namespace Varhall\Dbino\Tests;

use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Modifications tests
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class ModificationsTest extends AbstractDatabaseTest
{
    public function testNew()
    {
        $required = [
            'author_id' => 1,
            'title'     => 'Test',
            'written'   => new \Nette\Utils\DateTime(),
            'available' => FALSE,
            'price'     => 100.0
        ];
        
        $book = Book::instance($required);
        
        Assert::equal(FALSE, $book->available);
        Assert::equal(100.0, $book->price);
        Assert::equal('Test', $book->title);
    }
    
    public function testCreate()
    {
        $required = [
            'author_id' => 1,
            'title'     => 'Test',
            'written'   => new \Nette\Utils\DateTime(),
            'available' => FALSE,
            'price'     => 100
        ];
        
        $book = Book::create($required);
        Assert::equal(7, $book->id);
        
        $dbBook = Book::find(7);        
        Assert::equal(7, $dbBook->id);
        Assert::equal(FALSE, $dbBook->available);
        Assert::equal(100.0, $dbBook->price);
        Assert::equal('Test', $dbBook->title);
    }
    
    public function testCreateSave()
    {
        $book = new Book();
        $book->author_id = 1;
        $book->title = 'Test';
        $book->written = new \Nette\Utils\DateTime();
        $book->available = FALSE;
        $book->price = 100;
        
        $book->save();
        Assert::equal(8, $book->id);
        
        $dbBook = Book::find(8);        
        Assert::equal(8, $dbBook->id);
        Assert::equal(FALSE, $dbBook->available);
        Assert::equal(100.0, $dbBook->price);
        Assert::equal('Test', $dbBook->title);
    }
    
    public function testUpdate()
    {
        Book::find(1)->update([
            'title' => 'Changed'
        ]);
        
        $dbBook = Book::find(1);        
        Assert::equal(1, $dbBook->id);
        Assert::equal('Changed', $dbBook->title);
    }
    
    public function testUpdateSave()
    {
        $book = Book::find(1);
        $book->title = 'Changed';
        $book->save();
        
        $dbBook = Book::find(1);        
        Assert::equal(1, $dbBook->id);
        Assert::equal('Changed', $dbBook->title);
    }
    
    public function testSoftDelete()
    {
        Book::find(1)->delete();
        
        Assert::false(Book::find(1));
    }
    
    public function testRestore()
    {
        $book = Book::onlyTrashed()->get(1);
        
        Assert::equal(1, $book->id);
        Assert::true($book->isTrashed());
        
        $book->restore();
        Assert::false($book->isTrashed());
    }
    
    public function testForceDelete()
    {
        Book::find(1)->forceDelete();
        
        $book = Book::onlyTrashed()->get(1);
        Assert::false($book);
    }
    
    private function assertArray($array, $required)
    {
        $required = array_map(function($item) {
            return ($item instanceof \DateTime)
                        ? $item->format('c')
                        : $item;
        }, $required);
        
        foreach ($array as $key => $value) {  
            Assert::equal($required[$key], $value);
        }
        
        foreach ($required as $key => $value) {
            Assert::equal($array[$key], $value);
        }
    }
}

(new ModificationsTest())->run();
