<?php

namespace Varhall\Dbino\Tests;

use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Model tests
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class ModelTest extends AbstractDatabaseTest
{
    public function testFind()
    {
        $item = Author::find(1);

        Assert::equal('John', $item->name);
    }

    public function testFindNonExistent()
    {
        $item = Author::find(999);

        Assert::false($item);
    }
    
    public function testFindOrFailNonExistent()
    {
        Assert::exception(function() {
            Author::findOrFail(999);
        }, \Nette\InvalidArgumentException::class);
    }
    
    public function testFindDeleted()
    {
        $item = Author::find(4);

        Assert::false($item);
    }
    
    public function testGetSoftDeleted()
    {
        $book = Book::onlyTrashed()->get(6);
        
        Assert::equal(6, $book->id);
        Assert::equal(TRUE, $book->isTrashed());
    }
    
    public function testCustomProperty()
    {
        $item = Author::find(1);

        Assert::equal('John Smith', $item->fullName);
    }
    
    public function testToArray()
    {
        $array = Author::find(1)->toArray();
        $required = [
            'id'            => 1,
            'name'          => 'John',
            'surname'       => 'Smith',
            'web'           => 'http://www.smith.com/',
            'email'         => 'john@smith.com',
            'created_at'    => (new \DateTime('2017-10-20 13:35:02'))->format('c'),
            'updated_at'    => NULL,
            'deleted_at'    => NULL
        ];
        
        foreach ($array as $key => $value) {
            Assert::equal($required[$key], $value);
        }
        
        foreach ($required as $key => $value) {
            Assert::equal($array[$key], $value);
        }
    }
    
    public function testToJson()
    {
        $array = json_decode(Author::find(1)->toJson(), TRUE);
        $required = [
            'id'            => 1,
            'name'          => 'John',
            'surname'       => 'Smith',
            'web'           => 'http://www.smith.com/',
            'email'         => 'john@smith.com',
            'created_at'    => (new \DateTime('2017-10-20 13:35:02'))->format('c'),
            'updated_at'    => NULL,
            'deleted_at'    => NULL
        ];
        
        foreach ($array as $key => $value) {
            Assert::equal($required[$key], $value);
        }
        
        foreach ($required as $key => $value) {
            Assert::equal($array[$key], $value);
        }
    }
}

(new ModelTest())->run();
