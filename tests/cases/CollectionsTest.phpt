<?php

namespace Varhall\Dbino\Tests;

use Nette;
use Tester;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Collections tests
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class CollectionsTest extends AbstractDatabaseTest
{
    public function testAll()
    {
        $data = Author::all();
        
        Assert::equal(3, count($data));
        
        $required = [
            'John',
            'Martin',
            'Karoline',
        ];
        
        $i = 0;
        foreach ($data as $item) {
            Assert::equal($required[$i], $item->name);
            $i++;
        }
    }
    
    public function testWhere()
    {
        $data = Book::where('available', FALSE);
        
        Assert::equal(1, count($data));
        
        $item = $data->fetch();
        Assert::equal('PHP Tips & Tricks', $item->title);
    }
    
    public function testWhereIn()
    {
        $data = Book::where('id', [1, 2, 3]);
        
        Assert::equal(3, count($data));
        
        $i = 1;
        foreach ($data as $item) {
            Assert::equal($i, $item->id);
            $i++;
        }
    }
    
    public function testAllWhere()
    {
        $data = Book::all()->where('available', TRUE);
        
        Assert::equal(3, count($data));
        
        $required = [
            'MySQL Queries',
            'Einfach JavaScript',
            'Web programming',
        ];
        
        $i = 0;
        foreach ($data as $item) {
            Assert::equal($required[$i], $item->title);
            $i++;
        }
    }
    
    public function testWithTrashed()
    {
        $data = Author::withTrashed();
        
        Assert::equal(4, count($data));
        
        $required = [
            'John',
            'Martin',
            'Karoline',
            'Hans',
        ];
        
        $i = 0;
        foreach ($data as $item) {
            Assert::equal($required[$i], $item->name);
            $i++;
        }
    }
    
    public function testTrashedOnly()
    {
        $data = Author::onlyTrashed();

        Assert::equal(1, count($data));
        
        $required = [
            'Hans',
        ];
        
        $i = 0;
        foreach ($data as $item) {
            Assert::equal($required[$i], $item->name);
            $i++;
        }
    }

}

(new CollectionsTest())->run();
