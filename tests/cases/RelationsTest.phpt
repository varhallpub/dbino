<?php

namespace Varhall\Dbino\Tests;

use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Relations tests
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class RelationsTest extends AbstractDatabaseTest
{
    public function testOneToMany()
    {
        $data = Author::find(1)->books();

        $required = [
            'PHP Tips & Tricks',
            'MySQL Queries',
        ];
        
        $i = 0;
        foreach ($data as $item) {
            Assert::equal($required[$i], $item->title);
            $i++;
        }
    }
    
    public function testOneToManyDeleted()
    {
        $data = Author::find(2)->books();

        $required = [
            'Web programming',
        ];
        
        $i = 0;
        foreach ($data as $item) {
            Assert::equal($required[$i], $item->title);
            $i++;
        }
    }
    
    public function testManyToOne()
    {
        $item = Book::find(1)->author();

        Assert::equal('John', $item->name);
    }
    
    public function testManyToMany()
    {
        $data = Book::find(1)->tags();

        $required = [
            'MySQL',
            'XML',
            'PHP'
        ];
        
        Assert::equal(3, count($data));
        
        $i = 0;
        foreach ($data as $item) {
            Assert::equal($required[$i], $item->name);
            $i++;
        }
    }
}

(new RelationsTest())->run();
