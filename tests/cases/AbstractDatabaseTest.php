<?php

namespace Varhall\Dbino\Tests;

use Nette;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Abstract test class
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
abstract class AbstractDatabaseTest extends \Tester\TestCase
{
    /** @var \Nette\Database\Connection */
    public static $connection = NULL;
 
    /** @var \Nette\Caching\Storages\FileStorage */
    private static $cacheStorage = NULL;

    public function __construct()
    {
        $this->connectDatabase();
        $this->prepareTestData();
    }
    
    protected function configFile()
    {
        return __DIR__ . '/../mysql.neon';
    }

    /**
     * @return array
     */
    public function connectDatabase()
    {   
        if (!self::$connection) {
            $configLoader = new Nette\DI\Config\Loader();
            $config = $configLoader->load($this->configFile());

            if (is_file($this->configFile())) {
                $config = Nette\DI\Config\Helpers::merge($configLoader->load($this->configFile()), $config);
            }

            self::$connection = new \Nette\Database\Connection($config['database']['dsn'], $config['database']['user'], $config['database']['password']);
            
            $structure = new \Nette\Database\Structure(self::$connection, self::getCacheStorage());
            $conventions = new \Nette\Database\Conventions\DiscoveredConventions($structure);
            
            $context = new Nette\Database\Context(self::$connection, $structure, $conventions, self::getCacheStorage());
            
            \Varhall\Dbino\Model::initialize($context);
        }
        
        return self::$connection;
    }
    
    /** @return Nette\Caching\Storages\FileStorage */
    public static function getCacheStorage()
    {
        if (self::$cacheStorage === NULL) {
            self::$cacheStorage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../temp');
        }
        return self::$cacheStorage;
    }

    protected function prepareTestData()
    {
        \Nette\Database\Helpers::loadFromFile(self::$connection, __DIR__ . '/../database.sql');
    }
}
