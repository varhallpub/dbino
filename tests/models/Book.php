<?php

namespace Varhall\Dbino\Tests;

/**
 * Book test model
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class Book extends \Varhall\Dbino\Model
{
    public function author()
    {
        return $this->belongsTo(Author::class, 'author_id');
    }
    
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'book_tags', 'book_id', 'tag_id');
    }
 
    // configuration
    
    protected function attributeTypes()
    {
        return [
            'available' => 'bool',
            'price'     => 'float'
        ];
    }


    protected function table()
    {
        return 'books';
    }
}
