<?php

namespace Varhall\Dbino\Tests;

/**
 * Author test model
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class Author extends \Varhall\Dbino\Model
{
    public function books()
    {
        return $this->hasMany(Book::class, 'author_id');
    }
    
    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->surname}";
    }
    
    // configuration 
  
    protected function hiddenAttributes()
    {
        return [
            'password'
        ];
    }
    
    protected function table()
    {
        return 'authors';
    }

}
