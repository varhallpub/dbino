<?php

namespace Varhall\Dbino\Tests;

/**
 * Customer test model
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class Customer extends \Varhall\Dbino\Model
{
    
    public function users()
    {
        return $this->hasMany(User::class, 'customer_id');
    }
    
    protected function table()
    {
        return 'customers';
    }

}
