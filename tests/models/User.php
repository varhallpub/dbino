<?php

namespace Varhall\Dbino\Tests;

/**
 * User test model
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class User extends \Varhall\Dbino\Model
{
    protected function hiddenAttributes()
    {
        return [
            'password'
        ];
    }
    
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
    
    protected function table()
    {
        return 'users';
    }
}
